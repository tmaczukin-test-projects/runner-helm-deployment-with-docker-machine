variable "kubectl_config_path" {
  description = "Path to kubectl configuration file"
  type        = string
  default     = "~/.kube/config"
}

variable "kubectl_config_context" {
  description = "Kubectl config context to use"
  type        = string
  default     = "minikube"
}

variable "digitalocean_access_token" {
  description = "Token for accessing DigitalOcean's API"
  type        = string
  sensitive   = true
}

variable "runner_helm_repository" {
  description = "URL of the repository with Runner's Helm Chart"
  type        = string
  default     = "https://gitlab-runner-downloads.s3.amazonaws.com/helm-chart-beta"
}

variable "runner_helm_chart" {
  description = "Runner's Helm Chart to use"
  type        = string
  default     = "0.26.0-beta-1611176095-4-g4a90ab9"
}

variable "namespace" {
  description = "Kubernetes namespace in which deployment should be made"
  type        = string
  default     = "gitlab-runner"
}

variable "deployment_name" {
  description = "Name of the deployment"
  type        = string
  default     = "gitlab-runner"
}

variable "runner_gitlab_url" {
  description = "URL of GitLab instance"
  type        = string
  default     = "https://gitlab.com/"
}

variable "runner_registration_token" {
  description = "Runner's registration token"
  type        = string
  sensitive   = true
}