provider "helm" {
  kubernetes {
    config_path    = var.kubectl_config_path
    config_context = var.kubectl_config_context
  }
}

resource "helm_release" "gitlab_runner" {
  name      = var.deployment_name
  namespace = kubernetes_namespace.gitlab_runner.metadata[0].name

  repository = var.runner_helm_repository
  chart      = var.runner_helm_chart

  values = [
    file("values.yml"),
    <<EOF
gitlabUrl: ${var.runner_gitlab_url}
runnerRegistrationToken: ${var.runner_registration_token}

runners:
  config: |
    [[runners]]
      [runners.docker]
        privileged = false
      [runners.machine]
        IdleCount = 0
        MaxBuilds = 1
        MachineDriver = "digitalocean"
        MachineName = "minikube-%s"
        MachineOptions = [
          "digitalocean-access-token=${var.digitalocean_access_token}",
          "digitalocean-image=rancheros",
          "digitalocean-ssh-user=rancher",
          "digitalocean-region=nyc1",
          "digitalocean-size=s-2vcpu-2gb",
          "digitalocean-private-networking",
        ]

secrets:
- name: ${kubernetes_secret.docker_machine_tls.metadata[0].name}
  items:
  - key: ca_key
    path: ca-key.pem
  - key: ca_cert
    path: ca.pem
  - key: key
    path: key.pem
  - key: cert
    path: cert.pem
EOF
  ]
}
