# GitLab Runner Helm Chart deployment with Docker Machine in Digital Ocean

This project contains an example configuration for deploying GitLab Runner
to the Kubernetes cluster with the usage of the
[official Helm Chart](https://gitlab.com/gitlab-org/charts/gitlab-runner),
while using the Docker Machine executor for autoscaling.

The chosen cloud provider is [DigitalOcean](https://digitalocean.com).

## Requirements

1. [DigitalOcean](https://digitalocean.com) account and generated API token.

    **Be aware that usage of DigitalOcean, as any other public cloud, will
    cause costs. In real money.**

1. [GNU Make](https://www.gnu.org/software/make/) installed and available for local execution.

1. [Helm 3](https://helm.sh/) installed and available for local execution.

1. [Terraform](https://terraform.io/) installed and available for local execution.

1. A working [Kubernetes](https://kubernetes.io/) cluster. I've used local
   installation of [Minikube](https://kubernetes.io/docs/tasks/tools/#minikube)
   for my tests. However, any hosted cluster should work.

1. [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl) installed and available
   for local execution. When using Minikube, `kubectl` will be a requirement and will
   be automatically configured to access the cluster created by Minikube.

1. GitLab Runner's Helm Chart with these merge requests merged:

    - [Make executor configurable](https://gitlab.com/gitlab-org/charts/gitlab-runner/-/merge_requests/273)
    - [Allow to mount arbitrary KUbernetes secrets](https://gitlab.com/gitlab-org/charts/gitlab-runner/-/merge_requests/283)

    Before these are merged, the option to test this configuration is to
    clone the chart sources locally, merge the requested branches and run
    `helm package .`. Path to the generated `*.tgz` archive should be then
    set as the value of `runner_helm_chart` variable. More about variables
    will be said bellow.

1. A GitLab instance that will be accessible from the Kubernetes cluster and from DigitalOcean.
   For example [GitLab.com](https://gitlab.com/). The URL of GitLab instance will be needed.

1. A project at GitLab with CI/CD Pipelines enabled and with an example `.gitlab-ci.yml` file.

    A configuration like that will be enough for tests:

    ```yaml
    test runner:
      image: alpine:latest
      script:
      - echo "Hello world!"
      tags:
      - docker
    ```

1. Having the project the final thing that is needed is the runner registration token. It can be
   found at the _Settings -> CI/CD_ page from the _Runners_ section.

## Usage

1. Clone the project and enter the working directory:

    ```shell
    $ git clone https://gitlab.com/tmaczukin-test-projects/runner-helm-deployment-with-docker-machine
    Cloning into 'runner-helm-deployment-with-docker-machine'...
    (...)
    $ cd runner-helm-deployment-with-docker-machine/
    ```

1. Run `make prepare`. This will initialize terraform and will create
   a variables file named by your local username:

    ```shell
    $ make prepare
    # Initializing terraform

    Initializing the backend...

    Initializing provider plugins...
    - Finding hashicorp/kubernetes versions matching "~> 2.0"...
    - Finding hashicorp/helm versions matching "~> 2.0"...
    - Finding hashicorp/tls versions matching "~> 3.0"...
    - Installing hashicorp/kubernetes v2.0.1...
    - Installed hashicorp/kubernetes v2.0.1 (signed by HashiCorp)
    - Installing hashicorp/helm v2.0.2...
    - Installed hashicorp/helm v2.0.2 (signed by HashiCorp)
    - Installing hashicorp/tls v3.0.0...
    - Installed hashicorp/tls v3.0.0 (signed by HashiCorp)

    Terraform has created a lock file .terraform.lock.hcl to record the provider
    selections it made above. Include this file in your version control repository
    so that Terraform can guarantee to make the same selections by default when
    you run "terraform init" in the future.

    Terraform has been successfully initialized!

    You may now begin working with Terraform. Try running "terraform plan" to see
    any changes that are required for your infrastructure. All Terraform commands
    should now work.

    If you ever set or change modules or backend configuration for Terraform,
    rerun this command to reinitialize your working directory. If you forget, other
    commands will detect it and remind you to do so if necessary.
    # Creating variables file at tmaczukin.tfvars
    ```

1. Having the variables file created, fill it with required values:

    ```hcl
    digitalocean_access_token = "yourDigitalOceanAccessTokenHere"
    runner_registration_token = "runnerRegistrationTokenHere"
    ```

    If you want, you can overwrite default values of other variables. All variables are described
    below.

1. Run `make plan` to see what actions terraform will perform when configuration will be applied.

    Remember to run `make plan` each time before applying any changes.

1. Run `make apply` to apply the changes. Terraform will ask you to confirm that you want
   to perform the operation.

    You can use `make force-apply` to apply changes without need for manual confirmation.

1. Go to your GitLab project, open the _Settings -> CI/CD_ page and confirm that new runner
   was added at the _Runners_ tab. Confirm that the runner is tagged with `docker` tag.

1. Go to _CI/CD -> Pipelines_ page and manually start a new pipeline. In a moment you should
   see a running job. Quickly after that you should see in DigitalOcean that a new Droplet
   was created.

1. Wait for the job to finish. When it's done check DigitalOcean - the Droplet should be removed
   soon after the job is finished.

1. Run `make destroy` to destroy the configuration in your Kubernetes cluster. Terraform will
   ask you to confirm that you want to run the destroy.

1. Go do your GitLab project, open the _Settings -> CI/CD_ page and confirm that the runner
   was removed from the _Runners_ tab.

## Variables

| Variable                    | Type   | Default value         | Required | Description |
|-----------------------------|--------|-----------------------|----------|-------------|
| `kubectl_config_path`       | string | `~/.kube/config`      | no       | Path to kubectl configuration file. |
| `kubectl_config_context`    | string | `minikube`            | no       | Kubectl config context to use. |
| `digitalocean_access_token` | string | ``                    | yes      | Token for accessing Digital Ocean's API. |
| `runner_helm_repository`    | string | `https://gitlab-runner-downloads.s3.amazonaws.com/helm-chart-beta` | no       | URL of the repository with Runner's Helm Chart. By default the configuration points to Runner's official Helm Chart **beta repository**.  |
| `runner_helm_chart`         | string | `0.26.0-beta-1611176095-4-g4a90ab9` | no       | Runner's Helm Chart to use. Read bellow for detailed description. |
| `namespace`                 | string | `gitlab-runner`       | no       | Kubernetes namespace in which deployment should be made. |
| `deployment_name`           | string | `gitlab-runner`       | no       | Name of the deployment. |
| `runner_gitlab_url`         | string | `https://gitlab.com/` | no       | URL of GitLab instance. |
| `runner_registration_token` | string | ``                    | yes      | Runner's registration token. |

### `runner_helm_chart`

As it's described in the requirements list, at the moment of writing this README file (2021-01-28) there
are required features **that are not yet merged to the official Helm Chart**. This means that the default
configuration with Chart downloaded from the repository **will not yet work!**.

To make it working, as it was noted before, you need to clone the Helm Chart sources locally, merge the
branches from the related merge requests and package the Helm Chart manually with `helm package .`. This will
create an archive named like `gitlab-runner-0.26.0-beta.tgz`.

You can set the full path to this file as the value of `runner_helm_chart` variable. In that case
terraform will ignore the repository configuration and just use what is provided locally.

## Author

Tomasz Maczukin, 2021

## License

MIT
