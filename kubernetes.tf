provider "kubernetes" {
  config_path    = var.kubectl_config_path
  config_context = var.kubectl_config_context
}

resource "kubernetes_namespace" "gitlab_runner" {
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_secret" "docker_machine_tls" {
  metadata {
    name      = "docker-machine-tls"
    namespace = kubernetes_namespace.gitlab_runner.metadata[0].name
  }

  data = {
    ca_key  = tls_private_key.ca_key.private_key_pem
    ca_cert = tls_self_signed_cert.ca_cert.cert_pem
    key     = tls_private_key.docker_machine_key.private_key_pem
    cert    = tls_locally_signed_cert.docker_machine_cert.cert_pem
  }
}