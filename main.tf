terraform {
  required_providers {
    kubernetes = "~> 2.0"
    helm       = "~> 2.0"
    tls        = "~> 3.0"
  }
}
