variablesFile := $(USER).tfvars
helmOptions := -var-file=$(variablesFile)

.PHONY: prepare
prepare:
	# Initializing terraform
	@terraform init
	# Creating variables file at $(variablesFile)
	@touch $(variablesFile)

.PHONY: plan
plan:
	terraform plan $(helmOptions)

.PHONY: apply
apply:
	terraform apply $(helmOptions)

.PHONY: force-apply
force-apply:
	terraform apply -auto-approve $(helmOptions)

.PHONY: destroy
destroy:
	terraform destroy $(helmOptions)
