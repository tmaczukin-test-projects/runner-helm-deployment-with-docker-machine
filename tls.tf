resource "tls_private_key" "ca_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "tls_private_key" "docker_machine_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "tls_self_signed_cert" "ca_cert" {
  key_algorithm   = tls_private_key.ca_key.algorithm
  private_key_pem = tls_private_key.ca_key.private_key_pem

  validity_period_hours = 87600
  is_ca_certificate     = true

  allowed_uses = [
    "cert_signing",
    "crl_signing"
  ]

  subject {
    organization = "unknown"
  }
}

resource "tls_cert_request" "docker_machine_crs" {
  key_algorithm   = tls_private_key.docker_machine_key.algorithm
  private_key_pem = tls_private_key.docker_machine_key.private_key_pem

  subject {
    organization = "unknown"
  }
}

resource "tls_locally_signed_cert" "docker_machine_cert" {
  cert_request_pem   = tls_cert_request.docker_machine_crs.cert_request_pem
  ca_key_algorithm   = tls_private_key.ca_key.algorithm
  ca_private_key_pem = tls_private_key.ca_key.private_key_pem
  ca_cert_pem        = tls_self_signed_cert.ca_cert.cert_pem

  validity_period_hours = 87600

  allowed_uses = [
    "digital_signature",
    "client_auth"
  ]
}
